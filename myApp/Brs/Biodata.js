import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, ImageBackground, Image, SafeAreaView, TextInput, TouchableOpacity } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <View style={styles.bground} />
      <Text style={styles.title}>Biodata</Text>
      <SafeAreaView>
        <View style={styles.mid}>
          <View style={styles.kotak}>
            <Text style={styles.biodata}>Rian Soleh Irawan</Text>
            <Text style={styles.biodata}>Teknik Informatika</Text>
            <Text style={styles.biodata}>Pagi A</Text>
          </View>
          <Image source={require('./../Photo/BT.png')} style={styles.logo} />
        </View>
      </SafeAreaView>
      <TouchableOpacity style={styles.btnfb} onPress={() => window.location.href = '#'}>
        <Text style={styles.fbtext}>Rian Soleh </Text>
        <Image source={require('./../Photo/F.png')} style={styles.fb} />
      </TouchableOpacity>
      <TouchableOpacity style={styles.btnig} onPress={() => window.location.href = '#'}>
        <Text style={styles.igtext}>Rian Soleh.20 </Text>
        <Image source={require('./../Photo/ig.jpg')} style={styles.ig} />
      </TouchableOpacity >
      <TouchableOpacity style={styles.btnem} onPress={() => window.location.href = '#'}>
        <Text style={styles.emtext}>riansoleh44 </Text>
        <Image source={require('./../Photo/G.png')} style={styles.em} />
      </TouchableOpacity >
    </View >
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  kotak: {
    position: 'absolute',
    width: 300,
    height: 230,
    backgroundColor: '#DDDDDD10',
    borderWidth: 2,
    top: -495,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bg: {
    width: '100%',
    height: '100%'
  },
  input: {
    flex: 1,
    position: 'absolute',
    color: 'white'
  },

  logo: {
    position: 'absolute',
    top: -550,
    alignContent: 'center',
    justifyContent: 'center',
    height: 105,
    width: 105
  },

  bground: {
    backgroundColor: '#E5E5E5',
    width: 375,
    height: 800,
    opacity: 1,
  },

  title: {
    top: -580,
    right: 0,
    fontSize: 36,
    color: 'black'
  },
  title1: {
    top: -586,
    right: -5,
    fontSize: 40,
    color: 'black',
  },
  des: {
    bottom: 300,
    fontSize: 24,
    color: 'black',
    fontWeight: 'bold'
  },

  mid: {
    position: 'absolute',
    alignItems: 'center'
  },

  btnig: {
    position: 'absolute',
    borderWidth: 1,
    alignItems: 'center',
    top: 520,
    left: 200,
    backgroundColor: "white",
    width: 150,
    height: 36
  },

  login: {
    alignItems: 'center',
    fontSize: 21,
    color: 'white'
  },

  texusername: {
    position: 'absolute',
    borderRadius: 8,
    borderColor: 'white',
    borderWidth: 1,
    alignItems: 'center',
    bottom: 430,
    backgroundColor: "white",
    width: 262,
    height: 36
  },

  texpass: {
    position: 'relative',
    borderRadius: 8,
    borderColor: 'white',
    borderWidth: 1,
    alignItems: 'center',
    bottom: 410,
    backgroundColor: "white",
    width: 262,
    height: 36
  },
  tex: {
    top: 9,
    left: -52,
    fontSize: 13,
    color: 'white'
  },
  tex1: {
    position: 'relative',
    top: 9,
    left: -99,
    fontSize: 13,
    color: 'white'
  },
  btnfb: {
    position: 'absolute',
    borderWidth: 1,
    alignItems: 'center',
    top: 520,
    left: 30,
    backgroundColor: "white",
    width: 130,
    height: 36
  },
  Logo: {
    position: 'absolute',
    top: 2,
    left: -30,
    width: 30,
    height: 30
  },
  fbtext: {
    position: 'absolute',
    top: 2,
    left: 35,
    fontSize: 18,
    color: 'black'
  },
  fb: {
    width: 30,
    height: 30,
    position: 'absolute',
    top: 1,
    left: 5
  },
  igtext: {
    position: 'absolute',
    top: 2,
    left: 35,
    fontSize: 18,
    color: 'black'
  },
  ig: {
    width: 30,
    height: 30,
    position: 'absolute',
    top: 2,
    left: 2,
  },
  biodata: {
    fontSize: 20,
    marginTop: 15,
  },
  text: {
    position: 'absolute',
    top: 2,
    left: 35,
    fontSize: 18,
    color: 'black'
  },
  emtext: {
    position: 'absolute',
    top: 3,
    left: 30,
    fontSize: 18,
    color: 'black'
  },
  em: {
    width: 30,
    height: 30,
    position: 'absolute',
    top: 2,
    left: 0,
  },
  btnem: {
    position: 'absolute',
    borderWidth: 1,
    alignItems: 'center',
    top: 560,
    left: 120,
    backgroundColor: "white",
    width: 130,
    height: 36
  }

});

