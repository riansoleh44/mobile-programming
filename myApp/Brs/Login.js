import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, ImageBackground ,Image,SafeAreaView,TextInput, TouchableOpacity} from 'react-native';



export default function App() {
  const [text,onChangeText]=React.useState()
  
  const [text1,onChangeText1]=React.useState()
  return (
      
<View style={styles.container}>
  <StatusBar style="auto" />
  <View style={styles.bground}/>
   
    <View style= {styles.mid}>
      <Image source={require('./../assets/acc.png')} style= {styles.logo}/>
    </View>
    <View style= {styles.mid}>
      <Text style={styles.title}>Wellcome To</Text>
    </View>
    <View style= {styles.mid}>
      <Text style={styles.title1}>B.R.S</Text>
    </View>
      
    <SafeAreaView>
      
      
    <View style= {styles.mid}>
      <View style={styles.kotak}/>


    <View style= {styles.mid}>
      <View style= {styles.mid}>
        <SafeAreaView>
        <View style= {styles.mid}>
        <TextInput
            style={styles.texusername}
            placeholder=' Username'
            placeholderTextColor= 'black'
            onChangeText={onChangeText} value={text}/>
      </View>
          <TextInput
            style={styles.texpass}
            placeholder=' Password'
            placeholderTextColor= 'black'
            onChangeText1={onChangeText1} value={text1}/>
        </SafeAreaView>
      </View>
      <TouchableOpacity
          style={styles.btnlogin}     
          onPress={() => window.location.href = 'Login.js'}>
        <View style= {styles.mid}>
          <Text style= {styles.login}>Log in</Text>
        </View>
       
      </TouchableOpacity>
      <View style= {styles.mid}>
      <Text style={styles.des}>or </Text>
         </View>
    </View>  
      </View>
    </SafeAreaView>
    <TouchableOpacity
          style={styles.btngoogle}     
          onPress={() => window.location.href = 'Login.js'}>
        <View style= {styles.mid}>
          <Text style= {styles.google}>Sign Up With google</Text>
          <Image source={require('./../Photo/google.png')} style= {styles.Logo}/>
        </View>
       
      </TouchableOpacity>
</View>
);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  kotak:{
    position:'absolute',
    width:300,
    height:230,
    backgroundColor:'#DDDDDD10',
    borderColor: 'black',
    borderWidth:2,
    top:-495
  },
  bg: {
    width:'100%',
    height:'100%'
  },
  input: {
    flex:1,
    position: 'absolute',
    color: 'white'
  },

  logo: {
    position:'absolute',
    top : -480,
    alignContent:'center',
    justifyContent:'center',
    height: 100,
    width: 105
  },

  bground:{
    backgroundColor:'#E5E5E5',
    width: 375,
    height:800,
    opacity: 1,
  },

  title:{
    top:-580,
    right: 0,
    fontSize: 36,
    color: 'black'
  },
  title1:{
    top:-586,
    right: -5,
    fontSize: 40,
    color: 'black',
  },
  des: {
    bottom: 300,
    
    fontSize: 24,
    color: 'black',
    fontWeight: 'bold'
  },

  mid:{
    alignItems:'center'
  },

  btnlogin: {
    position:'absolute',
    borderRadius:100,
    borderColor: 'blue',
    borderWidth: 1,
    alignItems:'center',
    bottom: 350,
    left: 150,
    backgroundColor: "blue",
    width:100,
    height:36
  },

  login:{
    alignItems: 'center',
    fontSize: 21,
    color: 'white'
  },

  texusername: {
    position:'absolute',
    borderRadius:8,
    borderColor: 'white',
    borderWidth: 1,
    alignItems:'center',
    bottom : 430,
    backgroundColor: "white",
    width:262,
    height:36
  },

  texpass: {
    position: 'relative',
    borderRadius:8,
    borderColor: 'white',
    borderWidth: 1,
    alignItems:'center',
    bottom : 410,
    backgroundColor: "white",
    width:262,
    height:36
  },

  tex:{
    top: 9,
    left: -52,
    fontSize: 13,
    color: 'white'
  },

  tex1:{
    position: 'relative',
    top: 9,
    left: -99,
    fontSize: 13,
    color: 'white'
  },
  btngoogle:{
    position:'absolute',
    borderWidth: 1,
    alignItems:'center',
    top: 520,
    left: 55,
    backgroundColor: "white",
    width:250,
    height:36
  },
  google:{
    alignItems: 'center',
    fontSize: 21,
    color: 'black'
  },
  Logo:{
      position:'absolute',
      top:2,
      left:-30,
      width:30,
      height:30
  }

});

