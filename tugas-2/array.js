// 01
function range(startNum,finishNum){
array=[];
    if(startNum<finishNum){
        for(i=startNum; i<=finishNum; i++){
            array.push(i)
        }
    }else if(startNum==startNum && finishNum==null){
        array.push(-1)
    }else if(startNum==null && finishNum==null){
        array.push(-1)
    }else if(startNum>finishNum){
        for(j=startNum; j>=finishNum; j--){
            array.push(j)
        }
    }
    return array; 
    }

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1

//no 2
function rangeWithStep(startNum,finishNum,step){
    array=[];
    if(startNum<finishNum){
        for(i=startNum; i<=finishNum; i+=step){
            array.push(i)
        }
    }else if(startNum>finishNum){
        for(j=startNum; j>=finishNum; j-=step){
            array.push(j)
        }
    }
    return array;
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

//No 3
var jumlah = (a, b, c) => {
    var array = [];
    if (a == null && b == null && c == null) {
        array.push(0);
        var sum = array[0];
    }
    else if (a < b && c == null) {
        for (var i = a; i <= b; i++) {
            array.push(i);
        }
        var sum = array.reduce((a, b) => { return a + b; }, 0)
    }
    else if (a > b && c == null) {
        for (var j = a; j >= b; j--) {
            array.push(j);
        }
        var sum = array.reduce((a, b) => { return a + b; }, 0)
    }
    else if (a < b) {
        for (var k = a; k <= b; k += c) {
            array.push(k);
        }
        var sum = array.reduce((a, b) => { return a + b; }, 0)
    }
    else if (a > b) {
        for (var l = a; l >= b; l -= c) {
            array.push(l);
        }
        var sum = array.reduce((a, b) => { return a + b; }, 0)
    }
    else if (b == null && c == null) {
        array.push(1);
        var sum = array.reduce((a, b) => { return a + b; }, 0)
    }


    return sum;
}
console.log(jumlah(1, 10)) // 55
console.log(jumlah(5, 50, 2)) // 621
console.log(jumlah(15, 10)) // 75
console.log(jumlah(20, 10, 2)) // 90
console.log(jumlah(1)) // 1
console.log(jumlah()) // 0
console.log("");

//no 4
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989",
    "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermains Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970","Berkebun"]
]
for(i=0; i<input.length; i++){
    console.log("Nomor ID: "+input[i][0])
    console.log("Nama Lengakap: "+input[i][1])
    console.log("TTL: "+input[i][2])+input[i][3]
    console.log("Hobby : ")+input[i][4]
    console.log(" ")
}

//no 5
function balikKata(str) {
    var currentString = str;
    var newString = '';
    for (let i = str.length - 1; i >= 0; i--) {
        newString = newString + currentString[i];
    }
    return newString;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("Informatika")) // akitamrofnI
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Humanikers")) // srekinamuH ma I
console.log("");

//No 06
var dataHandling2 = (id, nama, alamat, tgl_lahir, hobi) => {
    array = [];
    array.push(id, nama, alamat, tgl_lahir, hobi);
    array.splice(1, 1, "Roman Alamsyah Elsharawy");
    array.splice(2, 1, "Provinsi Bandar Lampung");
    array.splice(4, 1, "Pria");
    array.splice(5, 0, "SMA Internasional Metro");
    format = array[3].split("/"); //["21","05","1989"]
    join = array[3].split("/"); //21-05-1989
    bln = format[1];
    switch (bln) {
        case "05": bln = "Mei"; break;
    }//Mei

    return array;
}
console.log(dataHandling2("0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"))
console.log(bln);
console.log(format.sort((a, b) => { return b - a }));
console.log(join.join("-"));
console.log(array[1].slice(0, 14));

