class Animal {
    constructor(name){
        this._animalName=name;
        this._animalLegs=4;
        this._animalCold_blooded=false;
    }
        get name(){
            return this._animalName;
        }
        set name(x){
            this._animalName=x;
        }
        get legs(){
            return this._animalLegs;
        }
        set legs(y){
            this._animalLegs=4;
        }
        get cold_blooded(){
            return this._animalCold_blooded;
        }
        set cold_blooded(z){
            this._animalCold_blooded;
        }
    }

    class Ape extends Animal{
        constructor(name){
        super(name);
        this._animalName=name;
        this._animalLegs=2;
    }
        yell(){
        console.log( "Auooo")
        }
    }
    class Frog extends Animal{
        constructor(name){
            super(name)
            this._animalName=name;
        }
        jump(){
            console.log("hop hop")
        }
    }
    var sheep = new Animal("shaun");
    console.log(sheep.name) // "shaun"
    console.log(sheep.legs) // 4
    console.log(sheep.cold_blooded) // false
    console.log(" ")

    var sungokong = new Ape("kera sakti")
    console.log(sungokong.name) // "shaun"
    console.log(sungokong.legs) // 4
    console.log(sungokong.cold_blooded) // false
    sungokong.yell() // "Auooo"
    console.log(" ")

    var kodok = new Frog("buduk")
    console.log(kodok.name) // "shaun"
    console.log(kodok.legs) // 4
    console.log(kodok.cold_blooded) // false
    kodok.jump() // "hop hop"

    class Clock {
        constructor({template}){
            this.template=template
    }
        render() {
        var date = new Date();
        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
        let output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
        console.log(output);
        }
        stop(){
        clearInterval(this.timer);
        }
        start(){
        this.render();
        this.timer = setInterval(()=>this.render(), 1000);
        }
        }
        var clock = new Clock({template: 'h:m:s'});
        clock.start();